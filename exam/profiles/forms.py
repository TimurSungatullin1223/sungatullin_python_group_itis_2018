from django.contrib.auth.models import User
from django.core import validators
from django import forms


class LoginForm(forms.ModelForm):
    password = forms.CharField(min_length=8, max_length=20, widget=forms.PasswordInput)
    email = forms.CharField(validators=[validators.validate_email])

    class Meta:
        model = User
        fields = ["email", "password"]