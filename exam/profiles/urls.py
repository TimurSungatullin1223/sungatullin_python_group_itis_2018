from django.urls import path

from profiles import views

urlpatterns = [
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout"),
    path('profile/<int:id>', views.profile, name="profile"),
    path('post/<int:id>/ajax', views.ajax_like, name="ajax"),
]
