from django.contrib import auth
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from django.urls import reverse

from profiles.forms import LoginForm
from profiles.models import Profile
from question.models import Answer, Question


def login(request):
    if request.user.is_authenticated:
        return redirect(reverse("profile", args=(request.user.id, )))
    form = LoginForm()
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            username = User.objects.get(email=email).username
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                auth.login(request, user)
                if "next" in request.GET:
                    return redirect(request.GET["next"])
                return redirect(reverse("profile", args=(request.user.id, )))
    return render(request, 'profiles/login.html', {"form": form})


@login_required
def logout(request):
    auth.logout(request)
    return redirect(reverse("login"))


def profile(request, id):
    user = request.user
    profile = get_object_or_404(Profile, pk=id)
    answers = Answer.objects.select_related("question").filter(question__user=profile)
    questions = profile.get_new_question()
    return render(request, 'profiles/profile.html', {"user": user,
                                                     "profile": profile,
                                                     "answers": answers,
                                                     "questions": questions})


@permission_required('admin')
def ajax_like(request, id):
    answer = Answer.objects.get(pk=id)
    count_like = answer.like + 1
    answer.like = count_like
    answer.save()
    return HttpResponse(count_like)



