from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Profile(models.Model):
    user = models.ForeignKey(User, related_name="profile_user", on_delete=models.CASCADE)

    def get_new_question(self):
        return self.question_user.filter(answer_question__answer__isnull=True)

    def __str__(self):
        return self.user.username
