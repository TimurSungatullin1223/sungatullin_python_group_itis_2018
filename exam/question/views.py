from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from django.urls import reverse

from profiles.models import Profile
from question.forms import AnswerForm
from question.models import Question, Answer


@login_required
def all_question(request):
    profile = get_object_or_404(Profile, user=request.user)
    questions = profile.get_new_question()
    return render(request, "question/question_list.html", {"questions": questions, "user": profile})


@login_required
def question_detail(request, id):
    question = get_object_or_404(Question, pk=id)
    if question.user_id != request.user.id:
        return redirect(reverse("login"))
    answer_form = AnswerForm()
    if request.POST:
        form = AnswerForm(request.POST)
        answer = form.save(commit=False)
        answer.question = question
        answer.save()
        return redirect(reverse("login"))
    return render(request, "question/question_detail.html", {"question": question, "form": answer_form})

