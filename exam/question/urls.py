from django.urls import path

from question import views

urlpatterns = [
    path('<int:id>', views.question_detail, name="question_detail"),
    path('', views.all_question, name="questions"),
]
