from django.forms import modelform_factory

from question.models import Answer

AnswerForm = modelform_factory(Answer, fields=['answer'])
