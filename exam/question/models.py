from django.contrib.auth.models import User
from django.db import models

from exam.settings import CUT_MSG_TEXT_LENGTH
from profiles.models import Profile


class Question(models.Model):
    question = models.TextField(verbose_name="Вопрос")
    user = models.ForeignKey(Profile,
                             related_name="question_user",
                             on_delete=models.CASCADE,
                             verbose_name="Пользователь")

    def __str__(self):
        return self.question[:CUT_MSG_TEXT_LENGTH]


class Answer(models.Model):
    answer = models.TextField(verbose_name="Ответ")
    question = models.ForeignKey(Question,
                                 related_name="answer_question",
                                 on_delete=models.CASCADE,
                                 verbose_name="Вопрос")
    like = models.IntegerField(default=0, verbose_name="Лайк")

    def __str__(self):
        return self.answer[:CUT_MSG_TEXT_LENGTH]
