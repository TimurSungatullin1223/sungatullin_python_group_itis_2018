import datetime

f = open("rasp.txt")
teachers = []
items = []
subjects = []


class Item:
    def __init__(self, weekday, t, subj, teacher):
        self.weekday = weekday
        self.time = t
        self.subj = subj
        self.teacher = teacher


weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
weekday = ""

for line in f:
    lines = line.split("\t")
    if line[:3] in weekdays:
        weekday = line[:3]
        continue
    teacher = lines[2].replace("\n", "")
    items.append(Item(weekday, lines[0], lines[1], teacher))
    if not lines[1] in subjects:
        subjects.append(lines[1])
    if not teacher in teachers:
        teachers.append(teacher)


def stat_hours(items):
    stat_teachers = dict.fromkeys(teachers)
    stat_subjects = dict.fromkeys(subjects)
    count = datetime.timedelta()
    for i in range(len(items)):
        delta = datetime.timedelta()
        timeD = items[i].time.split("-")
        time1 = datetime.datetime.strptime(timeD[0], "%H:%M")
        time2 = datetime.datetime.strptime(timeD[1], "%H:%M") if len(timeD) == 2 \
            else datetime.datetime.strptime(items[i + 1].time.split("-")[0], "%H:%M")
        delta += time2 - time1
        count += delta
        if stat_teachers[items[i].teacher] is None:
            stat_teachers[items[i].teacher] = delta
        else:
            stat_teachers[items[i].teacher] += delta
        if stat_subjects[items[i].subj] is None:
            stat_subjects[items[i].subj] = delta
        else:
            stat_subjects[items[i].subj] += delta
    print("The amount of work of each teacher")
    for k, v in stat_teachers.items():
        print("%s: %s" % (k, v))
    print("Study time in each subject")
    for k, v in stat_subjects.items():
        print("%s: %s" % (k, v))
    print("Total study time")
    print(count)


stat_hours(items)
