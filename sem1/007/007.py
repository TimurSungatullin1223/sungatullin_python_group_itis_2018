rimNum = input()


def convertNumber(rimNum):
    kw = {"M": 1000, "D": 500, "C": 100, "L": 50, "X": 10, "V": 5, "I": 1}
    arabicNum = 0
    count = 1
    mark = 0
    for n in rimNum:
        if n not in kw:
            print("This is not a Roman number (There is no such symbol) %s" % n)
            return
        if mark == 0:
            mark = kw.get(n)
            continue
        num = kw.get(n)
        if num > arabicNum + mark != mark:
            print("Roman numbers are written in descending order.")
            return
        if mark != num:
            count = 1
        if mark < num:
            if mark % 5 == 0 and mark % 10 != 0:
                print("Roman numerals (D, L, V) cannot be taken away.")
                return
            elif num % mark > 10:
                print("You can not take away through more than 2 discharges.")
                return
            else:
                arabicNum -= mark
                mark = num
                continue
        elif mark == num:
            if mark % 5 == 0 and mark % 10 != 0:
                print("These characters can not be near (%s)" % n)
                return
            count += 1
            if count > 3:
                print("You can not 4 consecutive identical numbers (%s)" % n)
                return
        arabicNum += mark
        mark = num
    print(arabicNum + mark)


convertNumber(rimNum)
