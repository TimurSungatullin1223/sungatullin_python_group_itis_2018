#команды notes нет

import os
import shutil
import re
import requests

path = os.path.abspath(".")
pathNew = ""


def copy(path, file):
    shutil.copyfile(path + "\\" + file, path + "\\" + file)


def validate_supercopy(f):
    def wrapper(s, path):
        if len(s) != 3:
            print("Incorrect number of arguments")
        else:
            if not os.path.exists(os.path.abspath(path + "\\" + s[1])):
                print("File '%s' not found." % s[1])
            elif not s[2].isdigit():
                print("%s is not a number." % s[2])
            else:
                n = int(s[2]);
                if n == 0:
                    print("Zero copies entered. Nothing to copy.")
                else:
                    f(s, path)

    return wrapper


@validate_supercopy
def process_supercopy(s, path):
    filename = s[1]
    n = int(s[2])
    size = len(s[2])
    k = filename.rfind(".");
    for i in range(1, n + 1):
        new_filename = filename[:k] + str(i).zfill(size) + "." + filename[k + 1:]
        shutil.copy(path + "\\" + filename, path + "\\" + new_filename)


def validate_grep(f):
    def wrapper(s, path):
        if len(s) == 3 or len(s) == 4:
            if len(s) == 4:
                if not s[2] == "-r":
                    print("Incorrect third argument")
                    return
            if os.path.exists(path + "\\" + s[-1]):
                f(s, path)
        else:
            print("Incorrect number of arguments")

    return wrapper


def r_process_grep(path, pat):
    for file in os.listdir(path):
        if os.path.isfile(path + "\\" + file):
            a = find(path + "\\" + file, pat)
            if len(a) != 0:
                print(find(path + "\\" + file, pat))
            print(find(path + "\\" + file, pat))
        elif os.path.isdir(path + "\\" + file):
            r_process_grep(path + "\\" + file, pat)


@validate_grep
def process_grep(s, path):
    new_path = path + "\\" + s[-1]
    if os.path.isfile(new_path):
        a = find(new_path, s[1])
        if len(a) != 0:
            print(find(new_path, s[1]))
    elif os.path.isdir(new_path):
        if len(s) == 4:
            r_process_grep(new_path, s[1])
        else:
            for file in os.listdir(new_path):
                if os.path.isfile(new_path + "\\" + file):
                    a = find(new_path, s[1])
                    if len(a) != 0:
                        print(find(new_path + "\\" + file, s[1]))


def process_wget(s):
    try:
        host = s[1]
        r = requests.get(host)
        if len(s) == 3:
            findFile(r.Text, s[2])
        else:
            download(host)
    except ConnectionError:
        print("Ой")
    else:
        print("Найс")


def findFile(a, extension):
    reg = r'href="[^"]+\.%s"' % extension
    for match in re.finditer(reg, a):
        s = str(match.group())
        s = s.replace("\"", "").replace("href=", "")
        download(s)


def download(url):
    with requests.get(url) as r:
        with open("htmlFile/" + url.split("/")[-1], 'wb') as f:
            f.write(r.content)


def find(path, pat):
    f = open(path)
    raws = []
    i = 0
    for line in f:
        i += 1
        k = line.rfind(pat)
        if k != -1:
            raws.append((i, k))
    return raws


def test(path):
    while True:
        path = os.path.abspath(path)
        # print(path + ": ", end="")
        command = input(path + ": ")
        if not command:
            continue
        s = command.split(" ")
        if s[0] == "ls":
            for file in os.listdir(path):
                print(file)
        elif s[0] == "cd":
            if s[1][0] == "/":
                pathNew = "C:" + s[1]
                if os.path.exists(pathNew):
                    if ".txt" == pathNew[-4:-1]:
                        f = open(os.path.abspath(pathNew))
                        for line in f:
                            print(line)
                    path = pathNew
                else:
                    print("Не удается найти указанный путь")
            else:
                pathNew = path + "\\" + s[1]
                if os.path.exists(pathNew):
                    path = pathNew
                else:
                    print("Не удается найти указанный путь")
        elif s[0] == "mkdir":
            pathNew = path + "\\" + s[1]
            if os.path.exists(pathNew):
                print("Невозможно создать файл, так как он уже существует")
            else:
                os.mkdir(pathNew)
        elif s[0] == "supercopy":
            process_supercopy(s, path)
        elif s[0] == "wget":
            process_wget(s)
        elif s[0] == "grep":
            process_grep(s, path)
        elif s[0] == "exit":
            return
        else:
            print("Такой команды нет")


test(path)
