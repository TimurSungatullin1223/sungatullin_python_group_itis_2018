def myDecorator(f):
    def checkType(*args):
        try:
            for i in range(len(args)):
                if not isinstance(args[i], int):
                    raise TypeError()
            f(*args)
        except TypeError:
            raise TypeError('This is not an integer: %d' % (i + 1)) from None
    return checkType


@myDecorator
def sumNum(*args):
    print(sum(args))


sumNum(123, 1231, 1234, 123.1)
