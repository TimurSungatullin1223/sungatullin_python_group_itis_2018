from bs4 import BeautifulSoup
from lxml import html
import re


def read_file(filename):
    with open(filename) as input_file:
        text = input_file.read()
    return text


def parse_user_soup(filename):
    results = []
    text = read_file(filename)
    # Beautiful Soup
    soup = BeautifulSoup(text, features="lxml")
    film_list = soup.find('div', {'class': 'profileFilmsList'})
    items = film_list.find_all('div', {'class': 'item', 'class': 'item even'})

    for item in items:
        movie_link = item.find('div', {'class': 'nameRus'}).find('a').get('href')
        movie_desc = item.find('div', {'class': 'nameRus'}).find('a').text
        movie_id = re.findall('\d+', movie_link)[0]
        name_eng = item.find('div', {'class': 'nameEng'}).text
        watch_datetime = item.find('div', {'class': 'date'}).text
        date_watched, time_watched = re.match('(\d{2}\.\d{2}\.\d{4}), (\d{2}:\d{2})', watch_datetime).groups()
        user_rating = item.find('div', {'class': 'vote'}).text
        if user_rating:
            user_rating = int(user_rating)
        results.append({
            'movie_id': movie_id,
            'name_eng': name_eng,
            'date_watched': date_watched,
            'time_watched': time_watched,
            'user_rating': user_rating,
            'movie_desc': movie_desc
        })
    return results


def parse_user_lxml(filename):
    results = []
    text = read_file(filename)

    tree = html.fromstring(text)

    film_list_lxml = tree.xpath('//div[@class = "profileFilmsList"]')[0]
    items_lxml = film_list_lxml.xpath('//div[@class = "item even" or @class = "item"]')
    for item_lxml in items_lxml:
        # getting movie id
        movie_link = item_lxml.xpath('.//div[@class = "nameRus"]/a/@href')[0]
        movie_desc = item_lxml.xpath('.//div[@class = "nameRus"]/a/text()')[0]
        movie_id = re.findall('\d+', movie_link)[0]

        # getting english name
        name_eng = item_lxml.xpath('.//div[@class = "nameEng"]/text()')[0]

        # getting watch time
        watch_datetime = item_lxml.xpath('.//div[@class = "date"]/text()')[0]
        date_watched, time_watched = re.match('(\d{2}\.\d{2}\.\d{4}), (\d{2}:\d{2})', watch_datetime).groups()

        # getting user rating
        user_rating = item_lxml.xpath('.//div[@class = "vote"]/text()')
        if user_rating:
            user_rating = int(user_rating[0])

        results.append({
            'movie_id': movie_id,
            'name_eng': name_eng,
            'date_watched': date_watched,
            'time_watched': time_watched,
            'user_rating': user_rating,
            'movie_desc': movie_desc
        })
    return results


filename = 'test.html'
