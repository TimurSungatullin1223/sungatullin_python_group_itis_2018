import requests
from bs4 import BeautifulSoup
import re

url = 'https://en.wikipedia.org/wiki/London'
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
}
r = requests.get(url, headers=headers)
text = r.text


def download(url):
    filename = url.split("/")[-1]
    filename = re.sub(r'\?.*', "", filename);
    filename = re.sub(r'#.*', "", filename);
    with requests.get(url, allow_redirects=True) as r:
        with open("pdfNew/" + filename, 'wb') as f:
            f.write(r.content)


def parse_user_soup(text):
    # Beautiful Soup
    soup = BeautifulSoup(text, features="lxml")
    cite_list = soup.find_all('cite', {'class': 'citation web'})

    for item in cite_list:
        pdf_desc = item.find('span', {'class': 'cs1-format'})
        if pdf_desc is not None:
            pdf_desc = pdf_desc.text
            if pdf_desc == "(PDF)":
                pdf_link = item.find('a').get('href')
                download(pdf_link)


parse_user_soup(text)
