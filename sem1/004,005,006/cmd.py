import os
import shutil
import re


class SuperFile:

    def __init__(self, path):
        path = os.path.abspath(path)
        if not os.path.isfile(path):
            with open(path, 'wb') as f:
                pass
        self.path = path

    def __add__(self, file):
        nameFirstFile = self.path.split("\\")[-1].split(".")
        nameSecondFile = file.path.split("\\")[-1].split(".")
        fileName = nameFirstFile[0] + nameSecondFile[0] + ".txt"
        newFile = SuperFile(os.path.dirname(self.path) + "\\" + fileName)
        with open(newFile.path, 'a') as nf:
            with open(self.path) as f:
                for line in f:
                    nf.write(line)
            nf.write("\n")
            with open(file.path) as f:
                for line in f:
                    nf.write(line)
        return file

    def __mul__(self, num):
        nameFile = self.path.split("\\")[-1].split(".")
        newNameFile = nameFile[0] + "mult%d" % num + "." + nameFile[1]
        file = SuperFile(os.path.dirname(self.path) + "\\" + newNameFile)
        with open(self.path) as f:
            lines = f.readlines()
        with open(file.path, 'a') as newFile:
            for i in range(num):
                for line in lines:
                    newFile.write(line)
        return file

    def println(self, s):
        with open(self.path, 'a') as f:
            f.write(s + "\n")

    def printAll(self):
        print("Файл: %s" % self.path.split("\\")[-1])
        with open(self.path) as f:
            for line in f:
                print(line, end="")


def test():
    path = os.path.abspath(".")
    while True:
        path = os.path.abspath(path)
        command = input(path + ": ")
        if not command:
            continue
        s = command.split(" ")
        if s[0] == "cd":
            if s[1][0] == "/":
                pathNew = os.path.abspath(s[1])
                if os.path.exists(pathNew):
                    path = pathNew
                else:
                    print("Не удается найти указанный путь")
            elif s[1]:
                pathNew = path + "\\" + s[1]
                if os.path.exists(pathNew):
                    path = pathNew
                else:
                    print("Не удается найти указанный путь")
        elif s[0] == "append":
            if s[1].split(".")[-1] != "txt":
                print("Это не текстовый файл, пожалуйста, используйте файл с расширением .txt")
                continue
            f = SuperFile(path + "\\" + s[1])
            f.printAll()
            while True:
                newStr = input()
                if newStr == "/exit":
                    check = input("Вы хотите выйти? (Y)es/(N)o ")
                    if check == "Y":
                        break
                f.println(newStr)
        elif s[0] == "rm":
            regular = s[1]
            files = []
            print("Найденные файлы")
            for file in os.listdir(path):
                if re.match(regular, file) is not None:
                    print(file)
                    files.append(file)
            print("Удалить? (Y)es/(N)o")
            check = input()
            if check == "Y":
                for file in files:
                    filePath = path + "\\" + file
                    if os.path.isfile(filePath):
                        os.remove(filePath)
                    else:
                        shutil.rmtree(filePath)
        elif s[0] == "exit":
            return
        else:
            print("Такой команды нет")


test()
file1 = SuperFile("test1.txt")
file2 = SuperFile("test2.txt")
file1 * 5
file1 + file2
