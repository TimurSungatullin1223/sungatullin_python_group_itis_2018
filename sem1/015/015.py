import requests
import re
import multiprocessing as mp


if __name__ == '__main__':
    mp.freeze_support()
    semaphore = mp.Semaphore(2)


    def find(a):
        reg = r'href="[^"]+\.pdf"'
        for match in re.finditer(reg, a):
            s = str(match.group())
            s = s.replace("\"", "").replace("href=", "")
            mp.Process(target=download(s)).start()


    def download(url):
        semaphore.acquire()
        if url[0] == "/":
            url = host + url
        with requests.get(url) as r:
            with open("pdf/" + url.split("/")[-1], 'wb') as f:
                f.write(r.content)
        semaphore.release()

    try:
        host = 'https://en.wikipedia.org/wiki/London'
        r = requests.get(host)
        find(r.text)
    except ConnectionError:
        print("Ой")
    else:
        print("Найс")


