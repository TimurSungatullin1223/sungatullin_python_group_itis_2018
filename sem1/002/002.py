class Vector2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "<%s, %s>" % (self.x, self.y)

    def __add__(self, v):
        return Vector2D(self.x + v.x, self.y + v.y)

    def __mul__(self, i):
        if type(i) == int:
            return Vector2D(self.x * i, self.y * i)
        else:
            return Vector2D(self.x * i.x, self.y * i.y)

    # def __copy__(self):
    #     print(self.x)

    def __sub__(self, v):
        return Vector2D(self.x - v.x, self.y - v.y)

    def __abs__(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def __eq__(self, v):
        return True if (self.x == v.x and self.y == v.y) else False
