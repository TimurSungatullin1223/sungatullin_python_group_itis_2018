from django.shortcuts import render

# Create your views here.
from django.utils import timezone

from mainapp.models import Twit


def index(request):

    if request.POST:
        twit = Twit(
            text=request.POST['text'],
            date=timezone.now()
        )
        twit.save()
        print(request.POST['text'])
    twits = reversed(Twit.objects.order_by("date"))
    print(twits)
    return render(request, 'index.html', {'twits': twits})