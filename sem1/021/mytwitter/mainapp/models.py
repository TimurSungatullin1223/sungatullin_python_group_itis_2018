from django.db import models


# Create your models here.

class Twit(models.Model):
    text = models.TextField(max_length=140)
    date = models.DateTimeField()

    def __str__(self):
        return self.text
