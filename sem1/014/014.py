import requests
import re


def find(a):
    reg = r'href="[^"]+\.pdf"'
    for match in re.finditer(reg, a):
        s = str(match.group())
        s = s.replace("\"", "").replace("href=", "")
        download(s)


def download(url):
    if url[0] == "/":
        url = host + url
    with requests.get(url) as r:
        with open("pdf/" + url.split("/")[-1], 'wb') as f:
            f.write(r.content)


try:
    host = 'https://en.wikipedia.org/wiki/London'
    r = requests.get(host)
    find(r.text)
except ConnectionError:
    print("Ой")
else:
    print("Найс")
