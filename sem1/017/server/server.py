import socket

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(2)
conn1, addr1 = sock.accept()
print('connected:', addr1)
conn2, addr2 = sock.accept()
print('connected:', addr2)
flag = True
a = ["Можно", "Нельзя"]

while True:
    if flag:
        conn1.send(a[0].encode("utf-8"))
        conn2.send(a[1].encode("utf-8"))
        data = conn1.recv(1024)
        if not data:
            break
        conn2.send(data)
        flag = False
    else:
        conn1.send(a[1].encode("utf-8"))
        conn2.send(a[0].encode("utf-8"))
        data = conn2.recv(1024)
        if not data:
            break
        conn1.send(data)
        flag = True
    print(data.decode("utf-8"))

conn1.close()
conn2.close()
