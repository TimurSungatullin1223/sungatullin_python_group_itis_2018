import socket

sock = socket.socket()
sock.connect(('localhost', 9090))

s = ""

while True:
    b = sock.recv(1024).decode("utf-8")
    if b == "Можно":
        print("Можете писать")
        s = input()
        if s == "end":
            break
        sock.send(s.encode('utf-8'))
    else:
        print("Сейчас пишет второй пользователь")
        data = sock.recv(1024)
        print("Собеседник пишет: %s" % data.decode("utf-8"))

sock.close()
