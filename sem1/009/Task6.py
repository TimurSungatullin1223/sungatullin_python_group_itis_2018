import pygame as p

p.init()
screen = p.display.set_mode((250, 100))
clock = p.time.Clock()
running = True
x = 0
i = -1
s = 25
step = 75
rect1 = [[10, 10], [10, s], [2 * s, s], [2 * s, 10]]
rect2 = [[step, 10], [step, s], [step + 2 * s, s], [step + 2 * s, 10]]
rect3 = [[step * 2, 10], [step * 2, s], [step * 2 + 2 * s, s], [step * 2 + 2 * s, 10]]
rects = [rect1, rect2, rect3]


def returnRect(rect):
    rect[0][0] += 5
    rect[0][1] += 5
    rect[1][0] += 5
    rect[1][1] -= 5
    rect[2][0] -= 5
    rect[2][1] -= 5
    rect[3][0] -= 5
    rect[3][1] += 5


def renderRect(rect):
    rect[0][0] -= 5
    rect[0][1] -= 5
    rect[1][0] -= 5
    rect[1][1] += 5
    rect[2][0] += 5
    rect[2][1] += 5
    rect[3][0] += 5
    rect[3][1] -= 5


renderRect(rects[i])
while running:
    clock.tick(3)
    screen.fill((255, 255, 255))
    for event in p.event.get():
        if event.type == p.QUIT:
            running = False
    if i < 2:
        i += 1
    else:
        i = 0
    p.draw.polygon(screen, (0, 255, 0), rect1)
    p.draw.polygon(screen, (0, 255, 0), rect2)
    p.draw.polygon(screen, (0, 255, 0), rect3)
    renderRect(rects[i])
    returnRect(rects[i - 1])

    p.display.flip()
