import pygame
pygame.init()
size = [500, 500]
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()
a = 0
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.fill((0, 0, 0))

    if a < 200:
        a += 1

    pygame.draw.rect(screen, (140, 140, 140), ((95, 195), (210, 60)))
    pygame.draw.rect(screen, (0, 255, 0), ((100, 200), (a, 50)))
    pygame.display.flip()
    clock.tick(30)
pygame.quit()