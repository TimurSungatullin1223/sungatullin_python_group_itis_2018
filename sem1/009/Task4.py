import pygame as p

p.init()
screen = p.display.set_mode((500, 500))
clock = p.time.Clock()
running = True
x = 0

while running:
    for event in p.event.get():
        if event.type == p.QUIT:
            running = False
    screen.fill((255, 255, 255))
    p.draw.circle(screen, (0, 255, 0), [250, 250], 100 + x)

    if x <= 0:
        x = 150
    x -= 10
    p.display.flip()
    clock.tick(30)
