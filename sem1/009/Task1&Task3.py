import pygame
import math

pygame.init()
size = [600, 400]
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()
myFont = pygame.font.SysFont("Times New Roman", 30)
percent = 0
angle = 0
running = True
GREEN = (0, 255, 0)
rect = (25, 25, 125, 125)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
locationText = (55, 65)
p = math.pi / 6
center = 200
c = 200
points1 = [[500, 500], [400, 400], [400, 300], [600, 300], [600, 400]]
points2 = [[500, 500], [600, 600], [600, 700], [400, 700], [400, 600]]
points1 = [[c, c], [c - 25, c - 25], [c - 25, c - 50], [c + 25, c - 50], [c + 25, c - 25]]
points2 = [[c, c], [c + 25, c + 25], [c + 25, c + 50], [c - 25, c + 50], [c - 25, c + 25]]
x = 0


def render(points):
    for i in range(1, 5):
        point0 = center + (points[i][0] - center) * math.cos(p) - \
                 (points[i][1] - center) * math.sin(p)
        point1 = center + (points[i][0] - center) * math.sin(p) + \
                 (points[i][1] - center) * math.cos(p)
        points[i][0] = point0
        points[i][1] = point1

    return points


while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.fill(WHITE)

    pygame.draw.arc(screen, GREEN, rect, 0, angle)
    if angle < 2 * math.pi:
        angle += 2 * math.pi / 100
        percent += 1
    else:
        angle = 0
        percent = 0
    text = myFont.render(str(percent) + " %", False, BLACK)
    screen.blit(text, locationText)

    pygame.draw.polygon(screen, GREEN, points1)
    pygame.draw.polygon(screen, GREEN, points2)
    points1 = render(points1)
    points2 = render(points2)

    pygame.display.flip()
    clock.tick(3)

pygame.quit()
