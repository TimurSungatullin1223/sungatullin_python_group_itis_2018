kw = {"M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90,
      "L": 50, "XL": 40, "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1}


def convertRim(func):
    def wrapper(*args):
        num = func(*args)

        rim = ""

        if num > 3999 or num < 0:
            print("The number must be from 0 to 4000")
            return num

        for k, v in kw.items():
            rim += num // v * k
            num %= v
        return rim

    return wrapper


@convertRim
def amount(*args):
    return sum(args)


s = amount(1000, 200, 80, 9)
print(s)