# Generic
class MyList(list):

    def __init__(self, listType):
        if isinstance(listType, type):
            super().__init__()
            self.listType = listType
        else:
            print("TypeError")

    def append(self, obj):
        try:
            if not isinstance(obj, self.listType):
                raise TypeError()
            else:
                list.append(self, obj)
        except TypeError:
            raise TypeError('This is not %s' % self.listType) from None


x = MyList(int)
x.append(1)
x.append(1)
x.append(1)
print(x)
x.append(1.1)
