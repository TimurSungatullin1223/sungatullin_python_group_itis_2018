class Type:
    def __init__(self, t, v):
        try:
            if not isinstance(t, type):
                raise TypeError()
            else:
                self.t = t
            if isinstance(v, t):
                self.v = v
            else:
                raise TypeError()
        except TypeError:
            raise TypeError('TypeError') from None

    def setValue(self, v):
        try:
            if not isinstance(v, self.t):
                raise TypeError()
            else:
                self.v = v
        except TypeError:
            raise TypeError('TypeError') from None

    def getValue(self):
        return self.v

    def __str__(self):
        return str(self.v)


x = Type(int, 5)
print(x)
x.setValue(10)
print(x)
