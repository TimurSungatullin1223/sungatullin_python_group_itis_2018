from django.db import models
from django.utils.timezone import now

from page.models import Page, DocumentsMixin, VerificationMixin
from profile_page.models import ProfilePage

STATUSES = (("O", "Open"), ("C", "Closed"), ("P", "Private"))


class Discussion(models.Model):
    text = models.TextField()


class Contact(models.Model):
    occupation = models.CharField(max_length=30)
    phone = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    name = models.CharField(max_length=35)
    profile_page = models.ForeignKey(ProfilePage, on_delete=models.CASCADE, null=True)


class Community(Page):
    class Meta:
        abstract = True

    title = models.CharField(max_length=15)
    information = models.TextField()
    discussions = models.ForeignKey(Discussion, on_delete=models.CASCADE, null=True)
    address = models.CharField(max_length=140, null=True)
    banner = models.TextField(null=True)
    admins = models.ManyToManyField(ProfilePage)
    contacts = models.ManyToManyField(Contact)


class EstDateMixin:
    est_date = models.DateField(default=now)


class Group(Community, EstDateMixin, DocumentsMixin):
    open_status = models.CharField(max_length=1, choices=STATUSES)


class Meeting(Community):
    date = models.DateTimeField()
    following = models.ManyToManyField(ProfilePage, related_name="following")
    un_following = models.ManyToManyField(ProfilePage, related_name="un_following")


class PublicPage(Community, EstDateMixin, VerificationMixin):
    meetings = models.ManyToManyField(Meeting)




