from django.urls import path

from community import views

app_name = "group"

urlpatterns = [
    path('<int:id>', views.page_detail, name='group')
]
