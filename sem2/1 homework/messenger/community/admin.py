from django.contrib import admin

# Register your models here.
from community.models import Discussion, Contact, Group, PublicPage, Meeting

admin.site.register(Discussion)
admin.site.register(Contact)
admin.site.register(Group)
admin.site.register(Meeting)
admin.site.register(PublicPage)

