from django.db.models import F, Q, Count
from django.http import HttpResponse
from django.shortcuts import render


# Create your views here.
from community.models import Group
from profile_page.models import ProfilePage


def page_detail(request, id):
    group = Group.objects.get(pk=id)
    avatar = group.page_picture.order_by("-date")[0]
    subs = group.subscribes.all()
    wall = group.walls
    if wall is not None:
        wall = group.walls.articles.all()
    return render(request, 'community/community.html',
                  {
                      'user': request.user,
                      'page': group,
                      'avatar': avatar,
                      'subs': subs,
                      'wall': wall
                  })
