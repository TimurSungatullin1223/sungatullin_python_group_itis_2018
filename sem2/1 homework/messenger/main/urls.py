from django.contrib import admin
from django.urls import path, include

from main import views

urlpatterns = [
    path("im/", views.show_message, name='show_message')
]
