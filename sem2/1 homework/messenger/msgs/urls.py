from django.contrib.auth.decorators import login_required
from django.urls import path

from msgs import views

app_name = "message"

urlpatterns = [
    path('im/', login_required(views.MyListView.as_view()), name='im'),
    path('im/<int:id>', login_required(views.CreateMsg.as_view()), name='detail'),
]
