from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import F
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic.list import ListView

from msgs.forms import MsgForm
from msgs.models import Msg


class MyListView(ListView):
    template_name = "msgs/im.html"
    model = Msg

    def get_context_data(self, **kwargs):
        context = super(MyListView, self).get_context_data(**kwargs)
        context["msgs"] = Msg.objects.filter(user_src=self.request.user) \
            .exclude(user_src=F("user_send")).order_by("-date")
        return context


class CreateMsg(CreateView):
    model = Msg
    form_class = MsgForm
    template_name = "msgs/msg.html"
    success_url = reverse_lazy("message:im")

    def get(self, request, *args, **kwargs):
        id = kwargs.get("id")
        msg = Msg.objects.get(pk=id)
        context = {'form': MsgForm, 'msg': msg}
        request.session["user_send"] = msg.user_send.pk
        return render(request, 'msgs/msg.html', context)

    def form_valid(self, form):
        msg = form.save(commit=False)
        msg.user_send = self.request.user
        msg.user_src = User.objects.get(pk=self.request.session["user_send"])
        msg.save()
        return HttpResponseRedirect(self.success_url)
