from msgs.models import Msg
from django import forms


class MsgForm(forms.ModelForm):
    class Meta:
        model = Msg
        fields = ["text"]
