from django.db import models
from django.utils.timezone import now


class Album(models.Model):
    title = models.CharField(max_length=50)


class Picture(models.Model):
    date = models.DateTimeField(default=now)
    img = models.ImageField(null=True, blank=True)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
