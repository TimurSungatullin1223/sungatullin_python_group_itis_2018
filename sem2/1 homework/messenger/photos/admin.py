from django.contrib import admin

# Register your models here.
from photos.models import Album, Picture

admin.site.register(Album)
admin.site.register(Picture)

