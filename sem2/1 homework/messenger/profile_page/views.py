from django.contrib import auth
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from django.http.response import HttpResponse
from django.shortcuts import render, redirect

from django.urls import reverse

from msgs.models import Msg
from profile_page.forms import LoginForm, PostForm
from profile_page.models import ProfilePage
from walls.models import Article


@login_required
def page_detail(request, id):
    page = ProfilePage.objects.get(pk=id)
    avatar = page.page_picture.order_by("-date")[0]
    q1 = Q(subscribes=page)
    q2 = Q(subscribe_to=page)
    q3 = q1 & q2
    q4 = q1 & ~q2
    friends = ProfilePage.objects.filter(q3)
    subs = ProfilePage.objects.filter(q4)
    wall = page.walls.articles.order_by("-date")
    groups = page.subscribe_to_groups.all()
    form = PostForm()
    if request.POST:
        f = PostForm(request.POST, request.FILES)
        article = f.save(commit=False)
        article.who_posted = page
        article.save()
        page.walls.articles.add(article)
        return redirect(reverse("people:profile_page", args=(id, )))

    return render(request, 'profile_page/profile_page.html',
                  {
                      'user': request.user,
                      'page': page,
                      'avatar': avatar,
                      'subs': subs,
                      'friends': friends,
                      'wall': wall,
                      'groups': groups,
                      'form': form
                  })


def login(request):
    if request.user.is_authenticated:
        return redirect(reverse("people:profile_page", args=(request.user.id, )))
    form = LoginForm()
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            username = ProfilePage.objects.get(user__email=email).user.username
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                auth.login(request, user)
                if "next" in request.GET:
                    return redirect(request.GET["next"])
                return redirect(reverse("people:profile_page", args=(request.user.id, )))
    return render(request, 'profile_page/login.html', {"form": form})


@login_required
def logout(request):
    auth.logout(request)
    return redirect(reverse("people:login"))


@login_required
def add_friend(request, id):
    page_friend = ProfilePage.objects.get(id=id)
    page = ProfilePage.objects.get(id=request.user.id)
    page.subscribe_to.add(page_friend)
    page_friend.subscribes.add(page)
    page_friend.save()
    page.save()
    return redirect(reverse("people:profile_page", args=(id, )))


@permission_required('admin')
def admin_view(request):
    msgs = Msg.objects.values_list("text", "date")
    articles = Article.objects.filter(like__gt=3)
    return render(request, "profile_page/admin.html", {
        "articles": articles,
        "msgs": msgs
    })


@permission_required('admin')
def ajax_like(request, id):
    article = Article.objects.get(pk=id)
    count_like = article.like + 1
    article.like = count_like
    article.save()
    return HttpResponse(count_like)
