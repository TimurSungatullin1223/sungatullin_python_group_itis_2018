from django import forms
from django.contrib.auth.models import User
from django.core import validators
from django.forms import modelform_factory

from walls.models import Article


class LoginForm(forms.ModelForm):
    password = forms.CharField(min_length=8, max_length=20, widget=forms.PasswordInput)
    email = forms.CharField(validators=[validators.validate_email])

    class Meta:
        model = User
        fields = ["email", "password"]


PostForm = modelform_factory(Article, fields=["text", "image"])
