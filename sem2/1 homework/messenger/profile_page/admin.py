from django.contrib import admin

# Register your models here.
from profile_page.models import ProfilePage

admin.site.register(ProfilePage)
