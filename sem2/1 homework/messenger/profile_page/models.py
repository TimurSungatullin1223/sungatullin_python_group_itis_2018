from django.contrib.auth.models import User
from django.db import models

from page.models import Page, VerificationMixin, DocumentsMixin


class ProfilePage(Page):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subscribe_to = models.ManyToManyField('self', related_name="subscribe_to")
    subscribe_to_groups = models.ManyToManyField('community.Group', related_name="subscribe_to_group")

