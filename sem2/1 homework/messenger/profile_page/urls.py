from django.contrib.staticfiles.urls import static
from django.urls import path
from django.views.generic.base import TemplateView

from messenger.settings import MEDIA_URL, MEDIA_ROOT
from profile_page import views

app_name = "people"

urlpatterns = [
    path('login/', views.login, name='login'),
    path('people/<int:id>', views.page_detail, name='profile_page'),
    path('logout/', views.logout, name='logout'),
    path('add/<int:id>', views.add_friend, name="add_friend"),
    path('people/admin', views.admin_view, name="admin_view"),
    path('post/<int:id>/ajax', views.ajax_like, name="ajax_like"),
    path('', TemplateView.as_view(template_name="profile_page/welcome.html"), name="welcome")
]

urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)
