from django.db import models
from django.utils.timezone import now

from documents.models import Document
from music.models import Music
from photos.models import Album, Picture
from videos.models import Video
from walls.models import Wall, Article


class Location(models.Model):
    country = models.TextField()
    city = models.TextField()


class Page(models.Model):
    class Meta:
        abstract = True

    creation_date = models.DateField(default=now)
    url_address = models.URLField(null=True, blank=True)
    status = models.CharField(max_length=100, null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    walls = models.ForeignKey(
        Wall,
        related_name="%(app_label)s_%(class)s_related",
        on_delete=models.PROTECT,
        default=None,
        null=True,
        blank=True
    )
    musics = models.ManyToManyField(Music, related_name="%(app_label)s_%(class)s_related")
    videos = models.ManyToManyField(Video, related_name="%(app_label)s_%(class)s_related")
    albums = models.ManyToManyField(Album, related_name="%(app_label)s_%(class)s_related")
    articles = models.ManyToManyField(Article, related_name="%(app_label)s_%(class)s_related")
    page_picture = models.ManyToManyField(Picture, related_name="%(app_label)s_%(class)s_related")
    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        null=True,
        related_name="%(app_label)s_%(class)s_related",
        blank=True
    )
    subscribes = models.ManyToManyField('profile_page.ProfilePage', related_name="%(app_label)s_%(class)s_related")


class VerificationMixin(models.Model):
    verification = models.NullBooleanField(default=False)


class DocumentsMixin(models.Model):
    documents = models.ManyToManyField(Document, default=None)
