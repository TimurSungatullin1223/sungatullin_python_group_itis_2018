from django.contrib import admin

# Register your models here.
from walls.models import Wall, Article

admin.site.register(Wall)
admin.site.register(Article)

