from django.contrib import admin

# Register your models here.
from documents.models import Document

admin.site.register(Document)
