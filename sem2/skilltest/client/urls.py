from django.urls import path, include

from client import views

urlpatterns = [
    path('create', views.create, name="create"),
    path('<int:id>', views.user, name="user")
]
