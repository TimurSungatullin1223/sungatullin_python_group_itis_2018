from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from client.forms import PostForm
from client.models import Client, Skill


def create(request):
    form = PostForm()
    if request.POST:
        f = PostForm(request.POST)
        skill = f.save()
        client = Client(
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            skills=skill
        )
        skill.save()
        client.save()
    return render(request, 'client/create_client.html', {"form": form})


def user(request, id):
    q1 = Q(pk=id)
    client = Client.objects.get(q1)
    print(client)
    client_skill = Skill.objects.values().get(pk=client.skills.pk)
    recommend = []
    l = list(map(lambda v: v[0], filter(lambda v: v[1] and not v[0] == "id", client_skill.items())))
    other_clients = Client.objects.filter(~q1).all()
    for i in other_clients:
        skills = Skill.objects.values().get(pk=i.skills.pk)
        l1 = list(map(lambda v: v[0], filter(lambda v: v[1] and not v[0] == "id", skills.items())))
        c = list(set(l) & set(l1))
        m = 2 * len(c) / (len(l1) + len(l))
        if m > 0.5:
            recommend.extend(list(set(l1) - set(l)))
    recommend = set(filter(lambda x: recommend.count(x) > len(other_clients) / 2, recommend))
    return render(request, 'client/friends.html', {"recommend": recommend})
