from django import forms
from .models import Skill


class PostForm(forms.ModelForm):

    class Meta:
        model = Skill
        fields = "__all__"
