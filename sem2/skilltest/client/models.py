from django.db import models


# Create your models here.
class Skill(models.Model):
    java = models.BooleanField()
    c_pluses = models.BooleanField()
    python = models.BooleanField()
    html = models.BooleanField()
    css = models.BooleanField()
    github = models.BooleanField()
    russian = models.BooleanField()
    english = models.BooleanField()
    french = models.BooleanField()
    django = models.BooleanField()


class Client(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    skills = models.OneToOneField(Skill, on_delete=models.CASCADE)

    def __str__(self):
        return self.last_name + self.first_name


